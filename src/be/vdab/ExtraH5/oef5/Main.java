package be.vdab.ExtraH5.oef5;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Type in a number: ");
        String number;
        while(true) {

            try {
                number = scanner.nextLine();
                isArmstrongNumber(number);
            } catch(RuntimeException e) {
                System.out.println("Type in a valid number: ");
            }
        }
    }

    public static void isArmstrongNumber(String num) {

        char[] chars = num.toCharArray();
        int sum = 0;

        for(int i = 0; i < chars.length; i++) {
            String stringNumber = String.valueOf(chars[i]);
            int intNumber = Integer.parseInt(stringNumber);
            System.out.println(intNumber);
            sum += Math.pow(intNumber, chars.length);
        }

        if(Integer.parseInt(num) == sum) {
            System.out.println("The number is an Armstrong number");
        } else {
            System.out.println("The number is not an Armstrong number");
        }
    }
}